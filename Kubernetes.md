What is Kubernetes?
    System for running many different containers in many different machines.
Why use kubernetes?
    When you need to run many different containers with different images.

Nodes + Master form a cluster 

kubectl - use for managing containers in the node
minikube(Only for Local) - use for managing the VM itself

# Kubernetes Architecture

# Master
- kube-api server: The API server acts as the front-end for kubernetes. The users, management devices, Command line interfaces all talk to the API server to interact with the kubernetes cluster.
- etcd: ETCD is a distributed reliable key-value store used by kubernetes to store all data used to manage the cluster. Think of it this way, when you have multiple nodes and multiple masters in your cluster, etcd stores all that
        information on all the nodes in the cluster in a distributed manner.
- kube-scheduler: The scheduler is responsible for distributing work or containers across multiple nodes. It looks for newly created containers and assigns them to Nodes.
- kube-controller-manager: The controllers are the brain behind orchestration. They are responsible for noticing and responding when nodes, containers or endpoints goes down. The controllers
    makes decisions to bring up new containers in such cases.
    1. Node Controller: Responsible for noticing and responding when nodes go down.
    2. Replication Controller: Responsible for maintaining the correct number of pods for every replication controller object in the system.
    3. Endpoints Controller: Populates the Endpoints object (that is, joins Services & Pods).
    4. Service Account & Token Controllers: Create default accounts and API access tokens for new namespaces.

# Node
- kubelet: kubelet is the agent that runs on each node in the cluster. The agent is responsible for making sure that the containers are running on the nodes as
           expected.
- kube-proxy: kube-proxy maintains network rules on nodes. These network rules allow network communication to your Pods from network sessions inside or outside of your cluster.
- Container Runtime: The container runtime is the software that is responsible for running containers. Like Docker, containerd

# Pod and Cluster
- Pod: A Kubernetes pod is a group of containers (sometime only one conainer)that are deployed together on the same host.
        1. Containers of same type, serving same purpose can't be in a same POD.
        2. Helper container which helps in process to another main container can be in same POD.
        3. In order to scale up the application, if we have to increase no.of containers each container should be in a diffe POD.
- Cluster: A Kubernetes cluster is a set of node machines for running containerized applications. If you’re running Kubernetes, you’re running a cluster.
            At a minimum, a cluster contains a worker node and a master node.

# Replication Controller:
    Controllers are the brain behind Kubernetes.

### High Availability ###
- What if for some reason, our application crashes and the POD fails? Users will no longer be able to access our application. 
- To prevent users from losing access to our application, we would like to have more than one instance or POD running at the same time. That way if one fails we still have our application running on the other one. 
- The *replication controller helps us run multiple instances of a single POD in the kubernetes cluster* thus providing High Availability.
- Even if you have a single POD, the replication controller can help by automatically bringing up a new POD when the existing one fails. 

### Load Balancing and Scaling ###
- Another reason we need replication controller is to create multiple PODs to share the load across them. 
- When the number of users increase we deploy additional POD to balance the load across the two pods. If the demand further increases and If we
were to run out of resources on the first node, we could deploy additional PODs across other nodes in the cluster. 
- The *replication controller spans across multiple nodes in the cluster. It helps us balance the load across multiple pods on different nodes* as well as scale our application when the demand increases.

## Replica Set ##
- There is one major difference between replication controller and replica set. 
- Replica set requires a **selector** definition. The selector section helps the replicaset identify what pods fall under it. But why would you have to specify what PODs fall under it, if
    you have provided the contents of the pod-definition file itself in the template? 
- It’s BECAUSE, *replica set can ALSO manage pods that were not created as part of the replicaset creation*. Say for example, there were pods created BEFORE the creation of
    the ReplicaSet that match the labels specified in the selector, the replica set will also take THOSE pods into consideration when creating the replicas.
- The selector is not a REQUIRED field in case of a replication controller, but it is still available. When you skip it, it assumes it to be the same as the labels provided in the pod-definition file. 
- In case of replica set a user input is required for this property. And it has to be written in the form of matchLabels. The matchLabels selector simply matches the labels specified under it to the labels on the
    PODs. The replicaset selector also provides many other options for matching labels that were not available in a replication controller.

# Deployment
- There are two types of Deployments:
    - Recreate Strategy: One way to upgrade 5 replicas to a newer version is to destroy all of these and then create newer versions of application instances. Meaning first, destroy the 5 running instances and then deploy 5 new
instances of the new application version. The problem with this as you can imagine, is that during the period after the older versions are down and before any newer
version is up, the application is down and inaccessible to users.
    - Rolling Strategy(Default Strategy): we take down the older version and bring up a newer version one by one. This way the application never goes down and the upgrade is seamless.

# Networking
- In Kubernetes the IP address is assigned to a POD. Each POD in kubernetes gets its own internal IP Address.
- When Kubernetes is initially configured it creates an internal private network with the IP address and all PODs are attached to it. When you deploy multiple PODs, they all get a
separate IP assigned. The PODs can communicate to each other through this IP.
- Unlike in the docker world where an IP address is always assigned to a Docker CONTAINER

## Cluster Networking
- There are different types of Network setups. we have to install them while creating kubernates cluster.
- The installed Network setup manages the networks and Ips in nodes and assigns a different network address for each network in the nodes. This creates a virtual network of all PODs and nodes where they
are all assigned a unique IP Address. And by using simple routing techniques the cluster networking enables communication between the different PODs or Nodes to
meet the networking requirements of kubernetes. Thus all PODs can now communicate to each other using the assigned IP addresses.