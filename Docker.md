# Docker

1. docker run image defaultcmd
    (Filesystem snapshot will be created in host and the defaultcmd runs by pointing to this FS in host hardware)
    (defaultcmd works only if it exists in the filesystem of the container that gonna be created from the image)
    (container will be created and defaultcmd will be executed after the execution the container exits)
    docker run = docker create + docker start
    docker create imagename defaultcmd

2. docker start containerid (we can start a exited container but the defaultcmd can't be replaced)
    docker start -a containerid ('-a' shows the output of this cmd)

3. docker logs containerid (To get the logs of container)

4. docker system prune (To delete all the continer that ever created. cache will be deleted)

5. docker stop containerid (To stop the running container)
   
   docker kill containerid (If there is a problem stopping the container docker eventually falls to kill cmd. even we can use to stop container immediately)

6. docker exec -it containerid anothercmd 
    
    (exec allows us to run anothercmd in the existing container)
    ('-it' is very imp. '-it' prompts terminal so we can give stdin and get stdout & stderr of the anothercmd (like entering into container))
    
    docker exec -it containerid sh (bash,powershell,zsh,sh are command processors. By these we can any get into container and perform anyoperation)
    
    docker run -it imagename sh (creates a container and enter into the container , run the required operations inside container) 

7. Baseimg-->create a container out it-->Run the next cmd(FS changed)-->New Image created outof temporary container and stored in cache (Take snapshot of temporary container FS)-->Shutdown temporary container 

8. docker commit -c 'defaultcmd' containerid (create a image by providing a defaultcmd out of container )

9. --volumes-from		Mount volumes from the other container(s)

# Dockerfile
docker build -f Dockerfile.dev . ('-f' to mention the dockerfile from which image gets build. (by default it searches for name 'Dockerfile'))

docker run -p HP:CP imagename
1. WORKDIR /usr/app (Any following cmd will be executed relative to this path)

2. COPY vs ADD
   
    COPY ./ ./ (Copying files from the 'src path to folder to copy from in HOST' to 'dest path to folder in container') (used to copy Build context to container)
   
    COPY copies a file/directory from your host to your image.
   
    ADD copies tar files from src to dest and extract them , but can also fetch packages from remote URLs.
    
3. CMD ["npm", "start"]
   
    RUN - RUN instruction allows you to install your application and packages required for it. It executes any commands on top of the current image and creates a new layer by committing the results. Often you will find multiple RUN instructions in a Dockerfile.
    
    CMD - CMD instruction allows you to set a default command, which will be executed only when you run container without specifying a command. If Docker container runs with a command, the default command will be ignored. If Dockerfile has more than one CMD instruction, all but last
      CMD instructions are ignored.
4. LABEL - Labels are a mechanism for applying metadata to Docker objects
    
    Ex: LABEL "author"="Saketh"
        LABEL "company"="Me & Co."



# Docker-Compose file:
    1. Seperate CLI that gets installed along with docker
    2. Used to startup multiple containers at the same time.
    3. Automates long commands we pass with 'docker run'
    
Docker Compose is used to run multiple containers as a single service. 
For example, suppose you had an application which required NGNIX and MySQL, you could create one file which would start both the containers as a service without the need to start each one separately.

docker-compose up = docker run imagename

docker-compose up --build = docker build locofdockfile + docker run imagname

docker-compose up -d (Creates a container in the background)

docker-comose down (stop containers)

docker-compose ps (List the containers, has to be run where the docker-compose file presents)


Restart Policies in Docker-compose file

    no : Never attempts to restart this container if it stops or crashes

    always : Always attempt to restart it if it stops for any reason

    on-failure : Only stops if the container stops with an error code

    unless-stopped : Always restart only if developers forcibly stops it 
    

# Docker Volumes
COPY in dockerfile will copy all the build context from a host location to a container. If we want to change any thing in build context it won't be reflected inside container.
So we use docker volumes, which creates a reference from container to location of required files in docker host 

docker run -p HP:CP -v HL:CL imagename (HL: Location of build context in host. CL: location in container)

docker run -p HP:CP -v /app/node_module -v HL:CL imagename (if we dont want to map a particular folder(here node_module) inside container to a location in host we use like bookmark the directory)