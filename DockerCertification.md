# Docker file:
- A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image.

- The docker build command builds an image from a Dockerfile and a context. Context is the set of files at a specified location PATH or URL. PATH is a directory on your local filesystem. URL is a Git repository location.
    1. Ex-1: docker build –t img_name .    (. Means current dir)
            . option is the context or source that the docker build command should use during the build.
    2. Ex-2: docker build –t img_name https://github.com/Dockerfile.git (context is git central repo)
            When building an image using a remote Git repository as build context, Docker performs a git clone of the repository on the local machine, and sends those files as build context to the daemon.

- ADD and COPY instruction look for files need to be added in image from context.

- You use the -f flag with docker build to point to a Dockerfile anywhere in your file system.
    1. mkdir -p dockerfiles context
    2. mv Dockerfile dockerfiles && mv hello.txt context
    3. docker build --no-cache -t helloapp:v2 -f dockerfiles/Dockerfile context

- You can specify a repository and tag at which to save the new image if the build succeeds.   Build context (.):
   docker build -t shykes/myapp .
   - tag the image into multiple repositories
     docker build -t shykes/myapp:1.0.2 -t shykes/myapp:latest .

# Dockerfile EX
    
- FROM ubuntu:18.04  (FROM creates a layer from the ubuntu:18.04 Docker image.)
- COPY . /app       (COPY adds files from your Docker client’s current directory.)
- RUN make /app      (RUN builds your application with make.)
- CMD python /app/app.py  (CMD specifies what command to run within the container.)
    

- All these layers are Read Only (R/O) layers
    1. When you create a new container, you add a new writable layer (R/W) on top of the underlying layers. This layer is often called the “container layer”. All changes made to the running container, such as writing new files, modifying existing files, and deleting files, are written to this thin writable container layer.
    2. The major difference between a container and an image is the top writable layer. When the container is deleted, the writable layer is also deleted. The underlying image remains unchanged.

- docker ps –s     (Container size on disk)
    1. size: the amount of data (on disk) that is used for the writable layer of each container.
    2. Virtual size: the amount of data used for the read-only image data used by the container plus the container’s writable layer size.

- When you use docker pull to pull down an image from a repository, each layer is pulled down separately, and stored in Docker’s local storage area, which is usually /var/lib/docker/ on Linux hosts. 

# Proxy server settings
- Proxy servers can block connections to your web app once it’s up and running. If you are behind a proxy server, add the following lines to your Dockerfile, using the ENV command to specify the host and port for your proxy servers:
- ENV http_proxy host_value:port_value (Set proxy server, replace host:port with values for your servers)

# DNS settings
DNS misconfigurations can generate problems with pip. You need to set your own DNS server address to make pip work properly. You might want to change the DNS settings of the Docker daemon. You can edit (or create) the configuration file at /etc/docker/daemon.json with the dns key
{
  "dns": ["your_dns_address", "8.8.8.8"]
}
The first element of the list is the address of your DNS server. The second item is Google’s DNS which can be used when the first one is not available.

# Share your image:
- Push to registries when you want to deploy containers to production. 
- A registry is a collection of repositories, and a repository is a collection of images.
- Note: We use Docker’s public registry here just because it’s free and pre-configured, but there are many public ones to choose from, and you can even set up your own private registry using Docker Trusted Registry.

- If you don’t have a Docker account, sign up for one at hub.docker.com.
    1.docker login (Log in to the Docker public registry on your local machine)
    2. docker tag image username/repository:tag (Tag image to upload the image to your desired destination)
    3. docker tag friendlyhello gordon/get-started:part2
    4. docker push username/repository:tag (Upload your tagged image to the repository)


- FROM python:2.7-slim  (Use an official Python runtime as a parent image)
- WORKDIR /app  (Set the working directory to /app)
- COPY . /app   (Copy the current directory contents into the container at /app)
- RUN pip install --trusted-host pypi.python.org -r requirements.txt    (Install any needed packages specified in requirements.txt)
- EXPOSE 80 (Make port 80 available to the world outside this container)
- ENV NAME World (Define environment variable)
- CMD ["python", "app.py"]  (Run app.py when the container launches)


## List Docker CLI commands
- docker container --help
- Display Docker version and info
    1. docker --version
    2. docker version
    3. docker info
- Execute Docker image
    1. docker run hello-world
- List Docker images
    1. docker image ls
- List Docker containers (running, all, all in quiet mode)
    1. docker container ls
    2. docker container ls --all
    3. docker container ls -aq

# docker-compose.yml file:
- A docker-compose.yml file is a YAML file that defines how Docker containers should behave in production. 
- After you have pushed the image you created to a registry. Create and Save this file as docker-compose.yml wherever you want. 

- version: "3"
- services:
  - web:
    - image: gordon/get-started:part2 (replace username/repo:tag with your name and image details)
    - deploy:
      - replicas: 5
      - resources:
        - limits:
          - cpus: "0.1"
          - memory: 50M
      - restart_policy:
        - condition: on-failure
    - ports:
      - "4000:80"
    - networks:
      - webnet
- networks:
  - webnet:
•	Run 5 instances of that image as a service called web, limiting each one to use, at most, 10% of a single core of CPU time (this could also be e.g. “1.5” to mean 1 and half core for each), and 50MB of RAM.
•	Immediately restart containers if one fails.
•	Map port 4000 on the host to web’s port 80.
•	Instruct web’s containers to share port 80 via a load-balanced network called webnet. (Internally, the containers themselves publish to web’s port 80 at an ephemeral port.)
•	Define the webnet network with the default settings (which is a load-balanced overlay network).

- Parser directives are written as a special type of comment in the form # directive=value. 
- All parser directives must be at the very top of a Dockerfile.
- escape=` (backtick)
- Setting the escape character to ` is especially useful on Windows, where \ is the directory path separator. If not specified, the default escape character is \.
- Environment variable substitution will use the same value for each variable throughout the entire instruction. In other words, in this example:
    - ENV abc=hello
    - ENV abc=bye def=$abc
    - ENV ghi=$abc
- will result in def having a value of hello, not bye. However, ghi will have a value of bye because it is not part of the same instruction that set abc to bye.

# .dockerignore file
Before the docker CLI sends the context to the docker daemon, it looks for a file named .dockerignore in the root directory of the context. If this file exists, the CLI modifies the context to exclude files and directories that match patterns in it. This helps to avoid unnecessarily sending large or sensitive files and directories to the daemon and potentially adding them to images using ADD or COPY.
comment
- Ex: */temp*, */*/temp*, temp?
- Lines starting with ! (exclamation mark) can be used to make exceptions to exclusions. 
    Ex: *.md, !README.md

#FROM
    - FROM instruction initializes a new build stage and sets the Base Image for subsequent instructions. 
    - FROM can appear multiple times within a single Dockerfile to create multiple images or use one build stage as a dependency for another.
    - Each FROM instruction clears any state created by previous instructions.
 - Optionally a name can be given to a new build stage by adding AS name to the FROM instruction. The name can be used in subsequent FROM and COPY --from=<name|index>instructions to refer to the image built in this stage.
- FROM <image>[:<tag>] [AS <name>]
- FROM <image>[@<digest>] [AS <name>]
- FROM instructions support variables that are declared by any ARG instructions that occur before the first FROM.
- ARG  CODE_VERSION=latest
- FROM base:${CODE_VERSION}
- CMD  /code/run-app
- FROM extras:${CODE_VERSION}
- CMD  /code/run-extras
- An ARG declared before a FROM is outside of a build stage, so it can’t be used in any instruction after a FROM.

#RUN
RUN has 2 forms:
- RUN <command> (shell form, the command is run in a shell, which by default is /bin/sh -c on Linux or cmd /S /C on Windows)
- RUN ["executable", "param1", "param2"] (exec form)
    - Ex: RUN ["/bin/bash", "-c", "echo hello"]
- RUN instruction will execute any commands in a new layer on top of the current image and commit the results. The resulting committed image will be used for the next step in the Dockerfile.
exec form makes it possible to avoid shell string munging, and to RUN commands using a base image that does not contain the specified shell executable.
default shell for the shell form can be changed using the SHELL command.
RUN /bin/bash -c 'source $HOME/.bashrc; \
echo $HOME'
The cache for RUN instructions isn’t invalidated automatically during the next build. The cache for an instruction like RUN apt-get dist-upgrade -y will be reused during the next build. The cache for RUN instructions can be invalidated by using the --no-cache flag, 
Ex: docker build --no-cache.
CMD
The CMD instruction has three forms:
•	CMD ["executable","param1","param2"] (exec form, this is the preferred form)
•	CMD ["param1","param2"] (as default parameters to ENTRYPOINT)
•	CMD command param1 param2 (shell form)
There can only be one CMD instruction in a Dockerfile. If you list more than one CMD then only the last CMD will take effect.
purpose of a CMD is to provide defaults for an executing container. 
Note: Unlike the shell form, the exec form does not invoke a command shell. This means that normal shell processing does not happen. For example, CMD [ "echo", "$HOME" ] will not do variable substitution on $HOME. If you want shell processing then either use the shell form or execute a shell directly, for example: CMD [ "sh", "-c", "echo $HOME" ]. When using the exec form and executing a shell directly, as in the case for the shell form, it is the shell that is doing the environment variable expansion, not docker.
Note: Don’t confuse RUN with CMD. RUN actually runs a command and commits the result; CMD does not execute anything at build time, but specifies the intended command for the image.
LABEL
LABEL <key>=<value> <key>=<value> <key>=<value> ...
The LABEL instruction adds metadata to an image. A LABEL is a key-value pair.
EXPOSE
EXPOSE <port> [<port>/<protocol>...]
The EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime. You can specify whether the port listens on TCP or UDP, and the default is TCP.
EXPOSE 80/udp (specify udp if you want udp protocol)
EXPOSE instruction does not actually publish the port. It functions as a type of documentation between the person who builds the image and the person who runs the container, about which ports are intended to be published. To actually publish the port when running the container, use the -p flag on docker run to publish and map one or more ports, or the -P flag to publish all exposed ports and map them to high-order ports.
ENV
ENV <key> <value>
ENV <key>=<value> ...
The ENV instruction sets the environment variable <key> to the value <value>.
You can view the values using docker inspect, and change them using 
docker run --env <key>=<value>.

ADD
ADD has two forms
•	ADD [--chown=<user>:<group>] <src>... <dest>
•	ADD [--chown=<user>:<group>] ["<src>",... "<dest>"] (this form is required for paths containing whitespace)
ADD instruction copies new files, directories or remote file URLs from <src> and adds them to the filesystem of the image at the path <dest>.
The <src> path must be inside the context of the build; you cannot ADD ../something /something, because the first step of a docker build is to send the context directory (and subdirectories) to the docker daemon.
ADD hom* /mydir/        # adds all files starting with "hom"
ADD hom?.txt /mydir/    # ? is replaced with any single character,e.g.,"home.txt"
COPY
COPY has two forms:
•	COPY [--chown=<user>:<group>] <src>... <dest>
•	COPY [--chown=<user>:<group>] ["<src>",... "<dest>"] (this form is required for paths containing whitespace)
COPY instruction copies new files or directories from <src> and adds them to the filesystem of the container at the path <dest>.
COPY test relativeDir/   # adds "test" to `WORKDIR`/relativeDir/
COPY test /absoluteDir/  # adds "test" to /absoluteDir/
The <src> path must be inside the context of the build; you cannot COPY ../something /something, because the first step of a docker build is to send the context directory (and subdirectories) to the docker daemon. <dest> ends with a trailing slash /
ENTRYPOINT
ENTRYPOINT has two forms:
•	ENTRYPOINT ["executable", "param1", "param2"] (exec form, preferred)
•	ENTRYPOINT command param1 param2 (shell form)
An ENTRYPOINT allows you to configure a container that will run as an executable.

VOLUME
VOLUME ["/var/log/"] or VOLUME /var/log /var/db
The VOLUME instruction creates a mount point with the specified name and marks it as holding externally mounted volumes from native host or other containers.
FROM ubuntu
RUN mkdir /myvol
RUN echo "hello world" > /myvol/greeting
VOLUME /myvol
This Dockerfile results in an image that causes docker run to create a new mount point at /myvol and copy the greeting file into the newly created volume.
The host directory (the mountpoint) is, by its nature, host-dependent. since a given host directory can’t be guaranteed to be available on all hosts. For this reason, you can’t mount a host directory from within the Dockerfile. The VOLUME instruction does not support specifying a host-dirparameter. You must specify the mountpoint when you create or run the container.
WORKDIR
WORKDIR /path/to/workdir
The WORKDIR instruction sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADDinstructions that follow it in the Dockerfile. If the WORKDIR doesn’t exist, it will be created even if it’s not used in any subsequent Dockerfile instruction.


