# Jenkins    
    1. Can we enable two types of SCM(Git, Mercurial) in Jenkins
    2. Can we override global credentials for any Job (YES)

# AWS
    1. Can we migrate EC2 from one VPC to another VPC?
    2. Can we create a Instace from AMI if the Snapshot is deleted ?

# Ansible
    1. If ansible nodes have different versions of Python installed, How can we proceed with Ansible

    
# Maven    
    1. Maven architecture
    2. POM.xml in maven
    3. Settings.xml in Maven
    

# Tomcat    
    1. Difference between WAR and EAR?
        EAR files are a superset containing WAR files and JAR files.
        WAR (Web Archive) is a module that gets loaded into a Web container of a Java Application Server. (Java Application Server has two containers - one is a Web container and the other is a EJB container.)
            A Web container requires the Web module to be packaged as a WAR file - that is a special JAR file with a web.xml file in the WEB-INF folder.
            EJB containers require EJB modules to be packaged as JAR files - these have an ejb-jar.xml file in the META-INF folder.
        EAR Enterprise applications may consist of one or more modules that can either be Web modules (packaged as a WAR file), EJB modules (packaged as a JAR file), or both of them. Enterprise applications are packaged as EAR files ― these are special JAR files containing an application.xml file in the META-INF folder.
        
    2. Where do we deploy EAR (EAR can't be delpoyed in Tomcat)
        EAR deployed to Application Server like websphere
    3. In single Tomcat how many catalina can be created ?
    4. Default log file name in Tomcat? (ANS: catalina.out)
    5. How to increase the size of tomcat? 
        set Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
    6. Can we install two middleware like Tomcat and Websphere in a single Machine?
    7. Can both the above applications work parallely ?
    8. Enable security for an application like tomcat
    9. How to find no.of files used by a PID or User ?
        lsof -p $PID