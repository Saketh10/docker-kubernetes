# Snapshot vs Release

The idea is that before a 1.0 release (or any other release) is done, there exists a 1.0-SNAPSHOT. That version is what might become 1.0. 
It's basically "1.0 under development". This might be close to a real 1.0 release, or pretty far (right after the 0.9 release, for example).
if you need a foo-1.0-SNAPSHOT.jar library, Maven will know that this version is not stable and is subject to changes. That's why Maven will try to find a newer version in the remote repositories, 
even if a version of this library is found on the local repository. However, this check is made only once per day.

<repository>
    <id>foo-repository</id>
    <url>...</url>
    <snapshots>
        <enabled>true</enabled>
        <updatePolicy>XXX</updatePolicy>
    </snapshots>
</repository>

where XXX can be:

always: Maven will check for a newer version on every build;
daily, the default value;
interval:XXX: an interval in minutes (XXX)
never: Maven will never try to retrieve another version. It will do that only if it doesn't exist locally. With the configuration, SNAPSHOT version will be handled as the stable libraries.


# SurefirePlugin:
The Surefire Plugin is used during the test phase of the build lifecycle to execute the unit tests of an application. It generates reports in two different file formats:

Plain text files (*.txt)
XML files (*.xml)
By default, these files are generated in ${basedir}/target/surefire-reports/TEST-*.xml.


#Two Types of Maven Plugins
1. Build plugins: They execute during the build process and should be configured in the <build/> element of pom.xml.

2. Reporting plugins: They execute during the site generation process and they should be configured in the <reporting/> element of the pom.xml.

# List of few common plugins −

Sr.No.	Plugin & Description
1. clean Cleans up target after the build. Deletes the target directory.

2. compiler Compiles Java source files.

3. surefire Runs the JUnit unit tests. Creates test reports.

4. jar Builds a JAR file from the current project.

5. war Builds a WAR file from the current project.

6. javadoc Generates Javadoc for the project.

7. antrun Runs a set of ant tasks from any phase mentioned of the build.


# MAVEN TUTORIAL NOTES

Maven : Java application management tool. Builds jar/war/ear. Run the application locally. Adde new dependencies. Run Unit tests.  Deploy to Tomcat/jetty/websphere.

.m2 is the directory in developer machine where local repo of maven is created

# Beginner Maven Project

- Build Life Cycle:
1. validate - validate the project is correct and all necessary information is available
2. compile - compile the source code of the project
3. test - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
4. package - take the compiled code and package it in its distributable format, such as a JAR.
5. verify - run any checks on results of integration tests to ensure quality criteria are met
6. install - install the package into the local repository, for use as a dependency in other projects locally
7. deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.

- Convention over Configuration:
1. src/main/java/App.java
2. src/test/java/AppTest.java
3. pom.xml

- mvn compile: compiles the main App.java file creates a App.class file in directory 'target/classes'
- mvn test-compile: Compiles both main App.java and test AppTest.java file in directory 'target/classes' and 'target/test-classes'
- mvn clean: To clean the 'target' directory
- mvn tomcat7:run does all the stages and deploy the war to tomcat

- POM: Project Object Model

-Name, version, packaging, dependencies, plugins
1. groupId,artifactId are part of the Naming our project uniquely
2. version has major , minor, and Incremental version like 1.0.2. 
3. packaging types are jar, war, ear and pom (for parent pom)
4. dependencies are projects depending on other projects there are transitive dependencies means dependencies of dependencies
    1. exclusions can be used to avoid unwanted transitive dependencies
    2. version of dependencies can be mention by range also like [4.0, 4.8], (4.1, 4.7)
    3. scope is where we want to use a dependency. The scopes are compile, import, provided, runtime, system, test 
5. plugins different types of plugins to serve different stage of build lifecycle
    a. source and target tags in plugins is to mention the java version to be used for the project


# Multi Module Project
- pom inherits from a super-pom/parent-pom
- When packaging is 'pom' it is considered as parent pom for all the other pom which defined in modules
- properties tag can be used like declaring variable for versions


# Tips and Tricks

1. mvn clean install
2. mvn -X clean install (prints a lot of information helps to debug) 
3. mvn effective:pom (prints the details of entire pom)
4. mvn help:effective-pom (displays the effective-pom together with pom)
5. mvn dependency:tree (prints entire depencencies with transitive dependencies)
6. mvn dependency:source (download sources for all the dependencies)
7. mvn help:effective-settings (shows the location of local maven repo)
8. mvn archetype(plugin):generate(goal) (generates new projects. Lot of archetypes are present we should use which is usefull for us to create a new project)
9. mvn install -DskipTests (skips test phase of execution)
