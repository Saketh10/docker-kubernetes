# IAM
In order to give permission to objects in S3 buckets, we can prefer either ACL or we can modify the resoure parameter to give perission at object level in bucket policy

Roles can be created and Policies will be attached to the roles

- Role :
    A set of permissions that can be granted temporarily. A user can set aside their original permissions temporarily and assume a role for a period of time between 15 minutes and 1 hour. Roles- What can I do?
- Policy :
    This is a JSON document describing permissions or rights that you can grant to a User, Group or Role that define what tasks users are allowed to perform. Policy – Who am I?


# Key Management Service
KMS service in AWS: We can create a key by which we can encrypt the data in S3 and EBS at rest. while the data is transfering from local to datacentre it will be encrypted.
while accessing the data, it will be decrypted automatically by AWS. No need to provide any key.


# AMI and Snapshot
Snapshot:
    A Snapshot is a backup of volume that's stored in S3.

When we create an AMI from an instance , a snapshot will be created for the EBS volume of the instance and the AMI and Snapshot are register together.
When we can create instance from that AMI, the EBS volume is refered to snapshot of this AMI hence the data is migrated between the intance volume.


# Cloudwatch vs CloudTrail
CloudWatch:
AWS CloudWatch is a suite of monitoring tools built into one AWS service. Cloudwatch monitors the Metrics, Alarms, Logs and Events
 
CloudTrail:
AWS CloudTrail is a log of every single API call that has taken place inside your Amazon environment. Each call is considered an event and is written in batches to an S3 bucket. 
These events show us details of the request, the response, the identity of the user making the request and whether the API calls came from the AWS Console, CLI, some third-party application or other AWS Service.

# Stopping vs terminating EC2
Stopping: Performs a normal shutdown on the instance and moves to a stopped state.
Terminating: Instance is moved to a shutdown state and its attached EBS volumes are deleted unless you set DeleteonTermination to 'False'

# Questions
1. AWS Services which are not region specific are IAM, Route 53, CloudFront, Web Application Firewall
2. Auto-scaling in AWS allows you to configure and automatically provision and launch new instances whenever the demand increases.
3. 